#include <ros/ros.h>
#include <hello_msgs/my_service.h>

int main(int argc, char **argv) {

	ros::init(argc, argv, "client") ;
	ros::NodeHandle node_handle ;

	// This creates a client
	ROS_INFO_STREAM("Client: a service client is created");

	// subscribe to the service "my_service" with type "hello_msgs::my_service"
	ros::ServiceClient client = node_handle.serviceClient<hello_msgs::my_service>("my_service");

	// make the service service
	hello_msgs::my_service srv;
	srv.request.a = 3 ;
	srv.request.b = 5 ;

	ROS_INFO_STREAM("Client: calling the service");

	// call the service
	if ( client.call(srv) ) {
		ROS_INFO_STREAM("Client: Sum = " << (long int)srv.response.sum);
	}
	else {
		ROS_ERROR("Failed to call service add_two_ints");
	}

	ros::spin() ;

	return 0;
}
