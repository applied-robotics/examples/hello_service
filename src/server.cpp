#include <ros/ros.h>
#include <hello_msgs/my_service.h>

bool my_call_back( hello_msgs::my_service::Request  &req,
				 	 hello_msgs::my_service::Response &res) {

	ROS_INFO_STREAM("Server: a=" << req.a << ", b=" << req.b);
	res.sum = req.a + req.b;
	ROS_INFO_STREAM("Server: sum=" << res.sum);

	return true;
}

int main(int argc, char **argv) {

	ros::init(argc, argv, "server");
	ros::NodeHandle node_handle ;

	// advertise a service call "my_service"
	ros::ServiceServer service = node_handle.advertiseService("my_service", my_call_back);

	ROS_INFO("Server: a server is created");
	ros::spin() ;


	return 0;
}
